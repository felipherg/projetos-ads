  
#include <stdio.h>

int verificarPalindromo(char palavra[20]){
  int i = 0;
  int j = 0;
  int k = 0;
  char outraPalavra[20];

  while(palavra[i] != '\0'){
      if(palavra[i] >= 65 && palavra[i] <=90)
          palavra[i] = palavra[i]+32;
    i++;
  }

  i--;

  while(i >= 0){
    outraPalavra[j] = palavra[i];
    j++;
    i--;
  }

  outraPalavra[j] = '\0';

  while(palavra[k] != '\0'){
    if(palavra[k] != outraPalavra[k]){
      return 0;
    }
    k++;
  }
  
  return 1;
}

int main(void) {

  char palavra[20];

  scanf("%s", palavra);

  int palindromo = verificarPalindromo(palavra);

  if(palindromo == 1){
    printf("%s é um palindromo", palavra);
  }else{
    printf("%s não é um palindromo", palavra);
  }
  return 0;
} 