#include <stdio.h>

void juntarString(char palavra[20], char outraPalavra[20]){
  int i = 0;
  int j = 0;
  while(palavra[i] != '\0'){
    i++;
  }
  while(outraPalavra[j] != '\0'){
    palavra[i] = outraPalavra[j];
    i++;
    j++;
  }
  palavra[i] = '\0';
}
int main(void) {

  char palavra[20];
  char outraPalavra[20];

  scanf("%s", palavra);
  scanf("%s", outraPalavra);

  juntarString(palavra, outraPalavra);

  printf("%s", palavra);

  return 0;
} 