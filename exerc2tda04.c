#include <stdio.h>

int tamanhoString(char palavra[20]){
  int i = 0;
  while(palavra[i] != '\0'){
    i++;
  }
  return i;
}
int main(void) {
  char palavra[20];

  scanf("%s", palavra);

  int tamanho = tamanhoString(palavra);

  printf("%d", tamanho);

  return 0;
} 