#include <stdio.h>

int checarString(char palavra[20], char outraPalavra[20]){
  int indice = 0;
  int resultado = 0;
  int tamanhoPrimeira = 0;
  int tamanhoSegunda = 0;

  while(palavra[indice] != '\0'){
    tamanhoPrimeira++;
    indice++;
  }
  indice = 0;

  while(outraPalavra[indice] != '\0'){
    tamanhoSegunda++;
    indice++;
  }
  if(tamanhoPrimeira != tamanhoSegunda){
    return 1;
  }
  indice = 0;

  while(palavra[indice] != '\0'){
    if(palavra[indice] != outraPalavra[indice]){
      resultado++;
    }
    indice++;
  }
  return resultado;
}
int main(void) {
  char palavra[20];
  char outraPalavra[20];

  scanf("%s", palavra);
  scanf("%s", outraPalavra);

  int resultado = checarString(palavra, outraPalavra);

  printf("%d", resultado);

  return 0;
}