#include <stdio.h>
#include <math.h>

int main(void) {

    int a0, razao, n;
    int termo;
    int cont;

    printf("Digite a0 razao n\n");
    scanf("%d %d %d", &a0, &razao, &n);

    cont = 0;
    while ( cont < n ) {
        termo = a0 * pow(razao, cont-1);
        printf("%d ", termo);
        cont = cont + 1;
    }
    printf("\n");
    return 0;
}