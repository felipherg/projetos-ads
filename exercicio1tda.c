#include <stdio.h>

int main (void )
{
  int valor;
  
  printf ("Digite um valor de 1 a 9: ");
  scanf("%d", &valor);
  
  switch ( valor )
  {
    case 0 :
    printf ("Fim de execução, adeus!: \n");
    break;
    
    case 1 :
    case 2 :
    case 3 :
    case 4 :
    case 5 :
    case 6 :
    case 7 :
    case 8 :
    case 9 :
    printf ("Digite um valor de 1 a 9: \n");
    break;
    
    default :
    printf ("Opção desejada é inexistente!\n");
  }
  
  return 0;
}