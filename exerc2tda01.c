#include <stdio.h>

int main(void) {
  char palavra[20];
  char copia[20];
  int indice = 0;

  scanf("%s", palavra);

  while(1){
    copia[indice] = palavra[indice];
    indice++;
    if(palavra[indice] == '\0'){
      copia[indice] = '\0';
      break;
    }
  }
  printf("%s", copia);
  return 0;
}