#include <stdio.h>
#include <string.h>

int main(void) {
  int m=0, n=0, soma, i;
  char impressao[1000];
  
  scanf("%d %d", &m, &n);
  while((m != 0) || (n != 0)){
    soma = m + n;
    sprintf(impressao, "%d", soma);
    for(i = 0; i < strlen(impressao); i++){
      if(impressao[i] - '0' != 0){
        printf("%c", impressao[i]);
      }
    }
    printf("\n");
    scanf("%d %d", &m, &n);
  }
  return 0;
}