#include <stdio.h>
#include <string.h>

int main(void) {
  int casos, valor1, valor2, i;
  char ordenado[100000], individual[100];
  
  scanf("%d", &casos);

  while(casos > 0){
    scanf("%d %d", &valor1, &valor2);
    for(i = valor1; i <= valor2 ; i++){
      sprintf(individual, "%d", valor1);
      strcat(ordenado, individual);
      valor1++;
    }
    printf("%s", ordenado);
    for(i = strlen(ordenado)-1; i>= 0;i--){
      printf("%c", ordenado[i]);
    }
    printf("\n");
    ordenado[0]= '\0';
    casos--;
  }
  return 0;
}