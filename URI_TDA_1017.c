#include <stdio.h>
 
int main() {
    int horas;
    int velocidadeMedia;
    float litrosPorHora;
    
    scanf("%d", &horas);
    scanf("%d", &velocidadeMedia);
    litrosPorHora = (horas * velocidadeMedia)/12.0;
    printf("%0.3f\n", litrosPorHora);
    return 0;
}